[![N|Solid](http://www.uoc.edu/portal/_resources/common/imatges/marca_UOC/UOC_Masterbrand_3linies.jpg)](http://www.uoc.edu/portal/ca/index.html)

# Activity Lifecycle

### Description
This project is to learn about how works the activity lifecycle. All the instructions are in the activity. 

### Getting Started:
##### Android Studio
Clone the project in Android Studio following these steps:
* Choose **VCS > Checkout from Version Control > GitHub** on the main menu.
* From the **Repository** drop-down list, select the source repository to clone the data from.
* In the **Folder** text box, specify the directory where the local repository for cloned sources will be set up.
* Click the Clone button to start cloning the sources from the specified remote repository.

##### Command line Git
Open your terminal, navigate to your working directory, use `git clone` to get a copy of the repo.

```
$ git clone https://eimtgit.uoc.edu/docente_android/android1-pec2-activitylifecycle.git
```

### Goals
The goals of this project are:
* Learn how lifecycle of the activities works.
* Learn what is an Intent.
* Know the difference between explicit and implicit intents.

### Links
* [The Activity Lifecycle]

### Sample
![Alt Text](https://eimtgit.uoc.edu/docente_android/android1-pec2-activitylifecycle/raw/master/resources/example_lifecycle_activities.gif)


[The Activity Lifecycle]: <https://developer.android.com/guide/components/activities/activity-lifecycle.html>